#include <iostream>
#include <vector>
#include <string>
#include <boost/compute.hpp>
#include <boost/math/constants/constants.hpp>
#include <random>



using namespace std;
namespace bc = boost::compute;

vector<float> *createSinewaveSignal(
       const unsigned int pps,
       const unsigned int points,
       const float peakAmplitude,
       const float frequency
);
vector<float> *addWhiteNoise(
       vector<float> *mySignal,
       const float standardDeviation
);
vector<float> *centeredMovingAverage(
       vector<float> *mySignal,
       unsigned int points
);

void writeSignalCSV(
       vector<float> *signal,
       const string filename,
       const unsigned int pps
);

int main() {
   const unsigned int pps=1800;
   vector<float> *mySignal= createSinewaveSignal(pps,5400,100.0f,2.0f);
   //cout << oclDevice.name() << endl;

   writeSignalCSV(mySignal,"sin.csv",pps);

   addWhiteNoise(mySignal,10.0f);

  writeSignalCSV(mySignal,"originalSignal.csv",pps);

  vector<float> *cma3=centeredMovingAverage(mySignal,3);
  writeSignalCSV(cma3,"cma3.csv",pps);

  vector<float> *cma5=centeredMovingAverage(mySignal,5);
  writeSignalCSV(cma5,"cma5.csv",pps);

  vector<float> *cma21=centeredMovingAverage(mySignal,21);
  writeSignalCSV(cma21,"cma21.csv",pps);


   return 0;
}

vector<float> *createSinewaveSignal(
       const unsigned int pps,
       const unsigned int points,
       const float peakAmplitude,
       const float frequency)
{
 vector<float> *mySignal= new vector<float>(points,0.0f);

float A=peakAmplitude;
float piNumber=boost::math::constants::pi<float>();
float omega=2.0f*piNumber*frequency;
 for(unsigned int i=0;i<points;i++)
 {
  float t=(float)i/(float)pps;
  (*mySignal)[i]=A*sin(omega*t);
 }
  return mySignal;
}

vector<float> *addWhiteNoise(vector<float> *mySignal,const float standardDeviation)
{
  default_random_engine eng;
   normal_distribution<float> dist(0.0f,standardDeviation);

   //float miNumeroAleatorio = dist(eng);

   for(unsigned int i=0;i<(*mySignal).size();i++)
   {
    (*mySignal)[i]+=dist(eng);
   }
  return mySignal;
}

//vector<float> *centeredMeanAverage(vector<float> *signal,unsigned int points)
//{

//}

void writeSignalCSV(vector<float> *mySignal,const string filename,const unsigned int pps)
{
  ofstream fs(filename);
  fs<<"time,data"<<endl;
  for(unsigned int i=0;i<(*mySignal).size();i++)
  {
    float t=(float)i/(float)pps;
    fs<<t<<","<<(*mySignal)[i]<<endl;
  }

  fs.close();
}

vector<float> *centeredMovingAverage(
       vector<float> *mySignal,
       unsigned int points
)
{
  const char programSourceCode[] = BOOST_COMPUTE_STRINGIZE_SOURCE(
// Put your code here
       __kernel void MeanKernel(__global float *mySignal,unsigned int points,unsigned int size, __global float *mySignalOut) 
       {

        unsigned int p =get_global_id(0);

        unsigned int media=points/2;
        if(p<media||p+media>=size) {
          mySignalOut[p]=mySignal[p];
        }
        else
        {
          float accum=0.0f;
          for(unsigned int i=p-media;i<=p+media;i++)
          {
            accum+=mySignal[i];
          }
          mySignalOut[p]=accum/(float)points;
        }
           return;
       }
       );


  bc::device oclDevice = bc::system::default_device();
   bc::context oclContext = bc::context(oclDevice);
   bc::command_queue oclQueue = bc::command_queue(oclContext,oclDevice);
   bc::program oclProgram =
           bc::program::create_with_source(programSourceCode, oclContext);
   oclProgram.build();
   bc::kernel oclMeanKernel =
           oclProgram.create_kernel("MeanKernel");


bc::vector<float> d_meanSignal(mySignal->begin(),mySignal->end(),oclQueue);
bc::vector<float> d_signalOut(mySignal->begin(),mySignal->end(),oclQueue);
vector<float> *h_meanSignal= new vector<float>(mySignal->size(),0.0f);
unsigned int size=d_meanSignal.size();
cout << size << endl;


oclMeanKernel.set_arg(0,d_meanSignal);
oclMeanKernel.set_arg(1,points);
oclMeanKernel.set_arg(2,size);
oclMeanKernel.set_arg(3,d_signalOut);

size_t workDims[1] = {size};
   oclQueue.enqueue_nd_range_kernel(
           oclMeanKernel,
           1,
           nullptr,
           workDims,
           nullptr);
   bc::copy(d_signalOut.begin(),d_signalOut.end(),h_meanSignal->begin(),oclQueue);
   oclQueue.finish();
   for(unsigned int k=0;k<10;k++) cout << (*h_meanSignal)[k] << ",";
   return h_meanSignal;



}


