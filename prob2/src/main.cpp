#include <iostream>
#include <boost/compute.hpp>
#include <vector>
#include <random>
#include <chrono>

using namespace std;
namespace bc = boost::compute;

void showMatrix(vector<float> *matrix, unsigned int matrixRows, unsigned int matrixCols);

int main() {

const char programSourceCode[] = BOOST_COMPUTE_STRINGIZE_SOURCE(
// Put your code here
       __kernel void rareKernel(
               __global float *matrixA,
               __global float *matrixB,
               __global float *matrixC,
               unsigned int rows,
               unsigned int cols) {
           unsigned int row = get_global_id(0);
           unsigned int col = get_global_id(1);
           int matrixsize_kernel = row*col;
           int accum;
           unsigned int r=row/matrixsize_kernel;
           unsigned int c=col%matrixsize_kernel;


           for(unsigned int k;k<matrixsize_kernel;k++)
           {
            accum += matrixA[c*matrixsize_kernel]*matrixB[k+matrixsize_kernel];


           }
           matrixC[row*cols+col]=accum;



           //matrixC[row*cols+col] *= 2;
           return;
       }
       );


   bc::device oclDevice =
           bc::system::default_device();
   bc::context oclContext =
           bc::context(oclDevice);
   bc::command_queue oclQueue =
           bc::command_queue(oclContext,oclDevice);
   bc::program oclProgram =
           bc::program::create_with_source(programSourceCode, oclContext);
   oclProgram.build();
   bc::kernel oclRareKernel =
           oclProgram.create_kernel("rareKernel");
   const unsigned int matrixRows = 4;
   const unsigned int matrixCols = 4;
   const unsigned int matrixSize = matrixRows*matrixCols;

   //generar las matrices para el kernel

   //matriz A
   vector<float> h_matrixA(matrixSize,0.0f);
   default_random_engine eng(chrono::system_clock::now().time_since_epoch().count());
   uniform_real_distribution<float> unif;
   for(unsigned int p = 0; p < matrixSize; p++)
       h_matrixA[p] = unif(eng);
   bc::vector<float> d_matrixA(h_matrixA.begin(),h_matrixA.end(),oclQueue);

   //matriz B
   vector<float> h_matrixB(matrixSize,0.0f);
   default_random_engine eng2(chrono::system_clock::now().time_since_epoch().count());
   uniform_real_distribution<float> unif2;
   for(unsigned int p=0;p<matrixSize;p++)
        h_matrixB[p]=unif2(eng2);
      bc::vector<float> d_matrixB(h_matrixB.begin(),h_matrixB.end(),oclQueue);

  //matriz C
  vector<float> h_matrixC(matrixSize,0.0f);
  bc::vector<float>d_matrixC(h_matrixC.begin(),h_matrixC.end(),oclQueue);







   cout << "MatrixA:" << endl;
   showMatrix(&h_matrixA,matrixRows,matrixCols);
   cout << endl;

   cout << "MatrixB:" << endl;
   showMatrix(&h_matrixB,matrixRows,matrixCols);
   cout << endl;

   cout << "MatrixC:" << endl;
   showMatrix(&h_matrixC,matrixRows,matrixCols);
   cout << endl;




   oclRareKernel.set_arg(0, d_matrixA);
   oclRareKernel.set_arg(1, d_matrixB);
   oclRareKernel.set_arg(2, d_matrixC);
   oclRareKernel.set_arg(3, matrixRows);
   oclRareKernel.set_arg(4, matrixCols);
   //oclRareKernel.set_arg(5, 2.0f);
   size_t workDims[2] = {matrixRows,matrixCols};
   oclQueue.enqueue_nd_range_kernel(
           oclRareKernel,
           2,
           nullptr,
           workDims,
           nullptr);
   bc::copy(d_matrixC.begin(),d_matrixC.end(),h_matrixC.begin(),oclQueue);




   cout << "After:" << endl;
   showMatrix(&h_matrixC,matrixRows,matrixCols);
   cout << endl;
   return 0;
}
void showMatrix(vector<float> *matrix, unsigned int matrixRows, unsigned int matrixCols) {
   for(unsigned int r = 0; r < matrixRows; r++) {
       for(unsigned int c = 0; c<matrixCols; c++) {
           cout << (*matrix)[r*matrixCols+c] << " ,";
       }
       cout << endl;
   }
}


